const BACKEND_URL = "http://54.234.162.218:5050/books";

let buttonCodeFirst = `
<div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Options
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
  `;
let buttonCodeSecond = `
  </div>
</div>`;

async function fetchData() {
  // document.getElementById('fetchButton').style.display = "none";

  const request = new Request(BACKEND_URL, {method: 'GET'});

  const response = await fetch(request);
  const result = await response.json();
  console.log(result);

  document.getElementById('bookList').innerHTML = "";

  let htmlCode = '';

  for (var i = 0; i < result.length; i++){

    htmlCode += '<tr>';
    htmlCode += '<td>' + result[i].title + '</td>';
    htmlCode += '<td>' + result[i].author + '</td>';
    htmlCode += '<td>' + result[i].price + '</td>';
    htmlCode += '<td>' + buttonCodeFirst;
    htmlCode += '<a class="dropdown-item" onclick="deleteData(\'' + result[i]._id + '\')">Delete</a>';
    htmlCode += '<a class="dropdown-item" onclick="updateData(\'' + result[i]._id +"\', \'"+ result[i].title +'\', \''+ result[i].author +'\', \''+ result[i].price +'\')">Update</a>';
    htmlCode += buttonCodeSecond + '</td>';
    htmlCode += '</tr>';

  }

  document.getElementById('bookList').innerHTML += htmlCode;
}


async function postData() {

  const data = {title: '', author: '', price: 0}
  data.title = document.getElementById('title').value;
  data.author = document.getElementById('author').value;
  data.price = document.getElementById('price').value;

  const request = new Request(BACKEND_URL, {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(data)
  });

  const response = await fetch(request);
  if(response.status === 400){
    console.log("Error");
  }
  else if(response.status === 500){
    console.log("Server Error");
  }
  fetchData();
}

async function deleteData(id) {

  const data = {id: ''}
  data.id = id;
  const request = new Request(BACKEND_URL, {
    method: 'DELETE',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(data)
  });

  const response = await fetch(request);
  if(response.status === 400){
    console.log("Error");
  }
  else if(response.status === 500){
    console.log("Server Error");
  }
  fetchData();
}

async function updateData(id, title, author, price) {
  const data = {id: '', title: title, author: author, price: price}
  data.id = id;

  newTitle = document.getElementById('title').value;
  newAuthor = document.getElementById('author').value;
  newPrice = document.getElementById('price').value;

  if(newTitle !== title && newTitle !== ''){
    data.title = newTitle;
  }
  if(newAuthor !== author && newAuthor !== ''){
    data.author = newAuthor;
  }
  if(newPrice !== price && newPrice !== ''){
    data.price = newPrice;
  }

  const request = new Request(BACKEND_URL, {
    method: 'PUT',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(data)
  });

  const response = await fetch(request);
  if(response.status === 400){
    console.log("Error");
  }
  else if(response.status === 500){
    console.log("Server Error");
  }
  fetchData();
}
