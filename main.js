const BACKEND_URL = "http://54.234.162.218:5050/books";

async function fetchData() {
  // document.getElementById('fetchButton').style.display = "none";

  const request = new Request(BACKEND_URL, {method: 'GET'});

  const response = await fetch(request);
  const result = await response.json();
  console.log(result);

  document.getElementById('bookList').innerHTML = "";

  let htmlCode = '';

  for (var i = 0; i < result.length; i++){

    htmlCode += "<div class=\"col-3 bookBlock mb-3\"><div class=\"bookCover\"><img class=\"imageClass\" src=\"bookCovers/book"+(i+1)+".jpg\" alt=\"Book\"></div>";
    htmlCode += "<div class=\"bookDescription\"><p class=\"titleText\">" + result[i].title + "</p>";
    htmlCode += "<p class=\"authorText\">" + result[i].author + "</p>";
    htmlCode += "<p class=\"priceText\">" + result[i].price + "</p>";
    htmlCode += "</div></div>";
  }

  document.getElementById('bookList').innerHTML += htmlCode;
}


async function postData() {

}

async function deleteData(id) {

}

async function updateData(id, title, author, price) {

}
